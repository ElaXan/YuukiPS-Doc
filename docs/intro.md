---
sidebar_position: 1
---

# Introduction

Welcome to YuukiPS, before you want to connect to our server there are a few things you need to know.

### Server Support

* GIO 3.2 Available for PC,Android,iOS
* GC  3.3 Available for PC,Android,iOS

### Server Requires:
* Patch RSAPatch for version 3.3+
* Patch UserAssembly for version 3.1-3.2
* Patch Metadata for version 2.8-3.0
(Tips: Don't mix, just use one of them.)

### Note
* All servers are free, if you use this server by paying from someone it means you are being scammed so be careful.
* Don't forget to 💎donate us so server doesn't die.
* Make sure to log in at least once every 1 month, so that your private server account is not deleted from server.