---
sidebar_position: 2
---

# Android

### Rename Folder from Official APK (No-Root) (3.3 GC)

This method use rename folder package name so you need game data from Official APK Launcher.

* Download Mod APK Launcher 3.3 (Don't install yet)
* Install "Official APK" and make sure you download game data first by playing for first time on official server. and don't forget to download language audio pack if needed (in-game).
* Download and install "Ziparchive".
* Open "Ziparchive" and find folder game data in "storage/emulated/0/Android/data" and look for folder name with "com.miHoYo.GenshinImpact" for official version.
* Rename "com.miHoYo.GenshinImpact" to "com.miHoYo.GenshinImpact.backup" (*Android 13 need root, others 11-12 don't need root)
Then Uninstall Official APK after that install Mod APK
Open "Ziparchive" and find folder game data in "storage/emulated/0/Android/data" and look for folder name with "com.miHoYo.GenshinImpact.backup" for official version.
* Rename "com.miHoYo.GenshinImpact.backup" to "com.miHoYo.GenshinImpact" (*Android 13 need root, others 11-12 don't need root)
* Open Game "Genshin Impact" to play private server ;)

### Copy Data Game (No-Root) (3.2 GIO)

It's the same method only you only need to download game data from us so you don't need to download from official server or use Official Launcher at all. 

> Note: **Running 3.2** and playing with **Official Server (3.3)** using **same Mod Launcher** won't work due to different versions. so you have to separate it using Version 1 so you can use a different private server package name with official server.

* Download Mod APK Launcher 3.2 V2
* Download Game Data 3.2 (make sure you have downloaded all parts)
* Download and install "Ziparchive".
* Open "Ziparchive" and unzip file com.miHoYo.ys.x.zip you can navigate/copy directly to Game Data in "storage/emulated/0/Android/data".
* Rename "com.miHoYo.ys.x" to "com.miHoYo.GenshinImpact" (*Android 13 need root, others 11-12 don't need root)
* Open Game "Genshin Impact" to play private server ;)

### Game Data 3.3 (GC)
> Part 4GB=meaning you have to download all of them) (All 22GB) (Includes English and Japanese Audio

Part1: EU1 | SG1 | Anonfiles | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 161B5B3F1776D2E9A8E4519B1F48A078)<br/>
Part2: EU1 | SG1 | Anonfiles | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: F9648E88377CF25222C4C1D5D3737FF4)<br/>
Part3: EU1 | SG1 | Anonfiles | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 1C66F500768AE3F0CB635F06399C449D)<br/>
Part4: EU1 | SG1 | Anonfiles | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: C87B741A0AFFA69488CDDC6E2C421D65)<br/>
Part5: EU1 | SG1 | Anonfiles | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 8B0410B80E8641279E15DBF77E200070)<br/>
Part6: EU1 | SG1 | Anonfiles | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 8C053C363D336E88945EC46FE38516C1)<br/>

> Note:<br/>
> Rename folder name to com.miHoYo.YuukiPS if using Launcher Version 2. 

### Game Data 3.2 (GIO)
> Part 4GB=meaning you have to download all of them (All 18GB) (Audio English only)

Part1: EU1 | SG1 | Anonfiles | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: C6138A8CF8B9E220825C64A6A1FB1BD4)<br/>
Part2: EU1 | SG1 | Anonfiles | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 1FF7B3AEB29C045B29E64D6B12412C01)<br/>
Part3: EU1 | SG1 | Anonfiles | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 1848D9343C3BBC8CA7B3931392DCA0CC)<br/>
Part4: EU1 | SG1 | Anonfiles | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 1AA48B9CBC359B321D7DDACCCFB8BEFC)<br/>
Part5: EU1 | SG1 | Anonfiles | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 262F0906C7B0FD76EE92ECB0F3055505)<br/>

> Note:<br/>
> If you are using Launcher Version 2 make sure you rename folder com.miHoYo.ys.x to com.miHoYo.GenshinImpact .
Remember

Make sure you check file with MD5 Checker Tool to make sure it has same value so that file you downloaded is original and not damaged.
Suggestions unzip this file via PC then copy it to /Android/data (Remember to make sure it's internal, not sdcard)

### Mod APK Launcher 3.3 (GC)
Version 1: [EU1](https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS.apk) | SG1 | [Anonfiles](https://anonfiles.com/Q1m4y5O9y0/YuukiPS_apk) | GD1-1 | [GD1-2](https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS.apk) | [GD2](https://file2.yuuki.me/GD2/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS.apk) | [GD3](https://file2.yuuki.me/GD3/Project/GenshinImpact/Data/Android/3.3.0/YuukiPS.apk) | [TeraBox](https://terabox.com/s/1nfg2l2G_u_2kse3nSceliA) | [Mediafire](https://www.mediafire.com/file/yq1pwcietv581d3/YuukiPS.apk/file) | [Mega](https://mega.nz/file/k08nQLaA#_I_8jfxDGvrNSIp39Ed1E0NC6RehqKAfZDJcRoHmmkM) | [OD1](https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS.apk)<br/>
Version 2: [EU1](https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS_V2.apk) | SG1 | [Anonfiles](https://anonfiles.com/8eocyfOeya/YuukiPS_V2_apk) | GD1-1 | [GD1-2](https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS_V2.apk) | [GD2](https://file2.yuuki.me/GD2/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS_V2.apk) | [GD3](https://file2.yuuki.me/GD3/Project/GenshinImpact/Data/Android/3.3.0/YuukiPS_V2.apk) | [TeraBox](https://terabox.com/s/1cE9SJVfrMB3qsqKFHoBSgQ) | [Mediafire](https://www.mediafire.com/file/98ipo58puxdngxv/YuukiPS_V2.apk/file) | [Mega](https://mega.nz/file/U4tVAIZK#fnoWQoy96PpJeS_FKnUPQdOnKTpE8JY2LIq7M8fsGng) | [OD1](https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS_V2.apk)<br/>

### Mod APK Launcher 3.2 (GIO)
Version 1: [EU1](https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Data/Android/3.2.0/Global/YuukiPS.apk) | SG1 | [Anonfiles](https://anonfiles.com/k4CcH8O0yf/YuukiPS_apk) | GD1-1 | [GD1-2](https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.2.0/Global/YuukiPS.apk) | [GD2](https://file2.yuuki.me/GD2/Project/GenshinImpact/Data/Android/3.2.0/Global/YuukiPS.apk) | [GD3](https://file2.yuuki.me/GD3/Project/GenshinImpact/Data/Android/3.2.0/Global/YuukiPS_V2.apk) | [TeraBox](https://terabox.com/s/1ObuxOR-8U15owV0FjBBSdQ) | [Mediafire](https://www.mediafire.com/file/mm5xjbteh84njsf/YuukiPS.apk/file) | [Mega](https://mega.nz/file/doN3nToI#yDPUJHsOryyJ9tByQ6unMNt8sz_WA5SjwKkVscqTOnQ) | [OD1](https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.2.0/Global/YuukiPS.apk)<br/>
Version 2: [EU1](https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Data/Android/3.2.0/Global/YuukiPS_V2.apk) | SG1 | [Anonfiles](https://anonfiles.com/B7B4H0O4y1/YuukiPS_V2_apk) | GD1-1 | [GD1-2](https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.2.0/Global/YuukiPS_V2.apk) | [GD2](https://file2.yuuki.me/GD2/Project/GenshinImpact/Data/Android/3.2.0/Global/YuukiPS_V2.apk) | [GD3](https://file2.yuuki.me/GD3/Project/GenshinImpact/Data/Android/3.2.0/Global/YuukiPS_V2.apk) | [TeraBox](https://terabox.com/s/13QKTKmSr9Jd-T45Ve7yhhQ) | [Mediafire](https://www.mediafire.com/file/qlabv8q62jyi41r/YuukiPS_V2.apk/file) | [Mega](https://mega.nz/file/00cCCIZA#ah7muZIgidyBWoVjFSeCfYAsWP1sOhCXUQnasEQF0Cc) | [OD1](https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS_V2.apk)<br/>

## FAQ

> #### Why stuck "Preparing to load data 0.00%"?
> Since you have passed what I have explained, FIRST YOU MUST DOWNLOAD GAME DATA FROM Official APK Launcher < never skip this. alternatively if you dont want to download from Official APK Launcher you can download Game Data from somewhere or backup from your other phone.

> #### What's difference between Version 1 and 2?
> V1 Can only connect to YuukiPS server.<br/>
> V2 Can Connect to LocalHost, Official Server (not working again since 3.3+), YuukiPS and better for Fix Problem (0kb) loading time.

> #### What's difference between GIO and GC?
> GIO is a clone of all functions from official server which means all functions work 100% without any bugs and quests all work but can only run on Version 3.2.<br/>
> GC is a server emulator that can run on latest versions and beta versions, but many functions don't work, it's just that commands are easier because they can be accessed via Ayaka Chat.

> #### Failed to load il2cpp
> Fix by delete folder "Unity","il2cpp" in Game Data Folder and enter game, the game opens without an error.

> #### Error 4214
> If you enter official server, which actually is not allowed to connect to official server using apk launcher mod because it is risky.
Server down or proxy still not working.


#### Update Link: 10/01/2023 (Add Game Data GIO via Terabox)